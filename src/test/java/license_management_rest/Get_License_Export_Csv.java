package license_management_rest;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_License_Export_Csv extends Util{
	
	ExtentTest test;
	@Test
	public void export_license_csv(Method method) throws IOException {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to download the License.Csv ");
		
		Response response = given().header("Authorization", getStagingToken())
				.queryParam("month", "JANUARY")
				.queryParam("year", "2020")
				.queryParam("timeZone", "America/Toronto")
				.get(ROOT_URI1 + "/license/export-csv");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		
		downloadFile(response, "license.csv");
	}

}
