package license_management_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_License_Update extends Util {
	ExtentTest test;

	@Test
	public void license_update(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to update the license ");

		String body = "{ \"billing\": { \"currentMonthCost\": null, \"farpointeCost\": null, \"licenseCost\": null,"
				+ " \"monthlyCost\": 0, \"yearlyCost\": null }, \"blueReaderEnabled\": true, \"licenseToken\": \"OB2C-TBJN-YRE5-04V2\","
				+ " \"noOfCredentials\": 50, \"specialFeature\": { \"farpointe\": true, \"farpointeOthers\": true }, \"userId\": \"samatha.sankeneni@dormakaba.com\"}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/license");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		test.log(LogStatus.PASS, "updated the License");

	}
}
