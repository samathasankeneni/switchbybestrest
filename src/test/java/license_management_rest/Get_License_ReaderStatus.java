package license_management_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_License_ReaderStatus extends Util {
	

	ExtentTest test;

	@Test
	public void get_readerStatus(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to get the license reader status");

		Response response = given().header("Authorization", getStagingToken())
				.queryParam("accountId", "t39581066")
				.get(ROOT_URI1 + "/license/reader-enable");
		
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		Boolean data = jsonPath.get("data");
		System.out.println(data);
		
		test.log(LogStatus.PASS, "License Reader status details are displayed");

	}

}
