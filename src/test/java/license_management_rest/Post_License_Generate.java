package license_management_rest;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_License_Generate extends Util {

	ExtentTest test;

	public void Generate_license() {
		

	String body = "{\"noOfCredentials\":50,\"specialFeature\":{\"farpointe\":true},\"blueReaderEnabled\":true,"
			+ "\"billing\":{\"licenseCost\":\"\",\"monthlyCost\":0},\"role\":\"admins\",\"userId\":\"samatha.sankeneni@dormakaba.com\","
			+ "\"partner\":{\"name\":\"samatha\",\"pocName\":\"Rest Assured\",\"enabled\":true,\"pocEmail\":\"restassured@gmail.com\","
			+ "\"pocTelephone\":\"+91456123789\",\"address\":{\"addressLine1\":\"750 marcel-Laurin\",\"addressLine2\":\"ST-laurent\","
			+ "\"city\":\"montreal\",\"state\":\"Quebec\",\"pincode\":\"51269\",\"country\":\"canada\"},\"id\":\"5e7e15151964c40001f8579a\","
			+ "\"lastUpdatedBy\":\"samatha\",\"createdBy\":\"samatha.sankeneni@dormakaba.com\",\"lastUpdatedOn\":1585330014708,\"createdOn\":1585321237480,"
			+ "\"pocId\":\"p111357673\",\"deleteFlag\":false,\"partnerCost\":5,\"isPartnerCost\":false}}";

	Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
			.post(ROOT_URI1 + "/");

	validateValues(response.getStatusCode(), 200, "verifying the status code",test);
	System.out.println(response.getStatusCode());
	JsonPath jsonPath = response.jsonPath();

	HashMap dataMap = jsonPath.get("data");
	// HashMap dataMap = jsonPath.get("data");
	System.out.println(dataMap.get("status"));
	System.out.println(dataMap.get("message"));

	test.log(LogStatus.PASS, "License Generated ");

	
	}
}
