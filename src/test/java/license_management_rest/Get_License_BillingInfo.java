package license_management_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_License_BillingInfo extends Util {

	ExtentTest test;

	@Test
	public void get_license_billingInfo(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to get the license billing info details");

		Response response = given().header("Authorization", getStagingToken()).get(ROOT_URI1 + "/license/billing");
		
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		//System.out.println(response.asString());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		
		test.log(LogStatus.PASS, "Billing Info details are displayed");

	}

}
