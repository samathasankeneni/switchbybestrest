package event_Info;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Event_Info extends Util {

	ExtentTest test;
	@Test
	public void get_event_info(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to get the Event Info");
		
		Response response = given().header("Authorization", getToken())
				.get(ROOT_URI1 + "/enventInfo");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		
		int listCounter=0;
		for(Object eachObject : dataList) {
			listCounter++;
			HashMap eachMap = (HashMap) eachObject;
			
			if(listCounter==1) {
				validateValues((Integer)eachMap.get("eventId"),1,"Verifying the eventId",test);
				validateValues(eachMap.get("eventName").toString(),"Switch� Core - Low Battery","Verifying the event Name",test);
				validateValues(eachMap.get("eventType").toString(),"BATTERY_LOW","Verifying the event Type",test);
			}
			else if(listCounter == 2) {
				
				validateValues((Integer)eachMap.get("eventId"),2,"Verifying the eventId",test);
				validateValues(eachMap.get("eventName").toString(),"Switch� Core - Battery Failure","Verifying the event Name",test);
				validateValues(eachMap.get("eventType").toString(),"BATTERY_FAIL","Verifying the event Type",test);
				
			}	
		
		}
			
		test.log(LogStatus.PASS, " verified Event Info Details");	
		
	}
}
