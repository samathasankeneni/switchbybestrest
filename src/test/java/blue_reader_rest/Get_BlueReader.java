package blue_reader_Rest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.Util;
import static io.restassured.RestAssured.*;

import java.util.ArrayList;
import java.util.HashMap;


public class Get_BlueReader extends Util{
	
	@Test
	public void get_bluereader() {
	    Response response = given().header("Authorization",getToken())
	            .queryParam("accountId","t17176877")
	            .queryParam("siteId","1583158009")
	            .get("https://uat.switchbybest.com/accounts/api/blueReader");

	    System.out.println("The Response code is:"+response.getStatusCode());
	    System.out.println(response.asString());
	    Assert.assertEquals(response.getStatusCode(),200,"verifying the status code");
	    JsonPath jsonPath=response.jsonPath();
	    HashMap dataList = jsonPath.get("data");
	    
	}
	
}