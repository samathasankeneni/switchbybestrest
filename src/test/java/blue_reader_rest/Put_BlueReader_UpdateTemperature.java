package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_BlueReader_UpdateTemperature extends Util{

	ExtentTest test;
	@Test
	public void update_temperature(Method method) {
		
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the BlueReader temperature");
		
		String body= "{ \"bluereaderUid\": 1222, \"millis\": \"1660\", \"temperature\": 25}";
	
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/blueReader/temperature");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
//        Assert.assertEquals(jsonPath.get("data"),true);
        validateValues((Boolean)jsonPath.get("data"),true, "Verifing the data", test);
        test.log(LogStatus.PASS, " updated Blue readers temperature");
        
        
	}
	
	
}
