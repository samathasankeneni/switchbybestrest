package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_BlueReader_byId extends Util {
	
	ExtentTest test;
	@Test
	public void get_bluereader(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "satrted to get the BlueReader details BY Id");
		
		Response response = given().header("Authorization", getToken()).queryParam("id", "5e7a6ae61964c40001f85524")
				.get(ROOT_URI1 + "/blueReader/details");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		
/* validating values*/
		
		validateValues(data.get("id").toString(), "5e7a6ae61964c40001f85524", "verifying the id",test);
		validateValues(data.get("name").toString(), "Switch Reader_3", "verifying the name",test);
		validateValues((Boolean)data.get("perimeterCheckin"), true, "verifying perimeterCheckin",test);
		validateValues((Boolean)data.get("multifactor"), true, "verifying multifactor",test);
		validateValues(data.get("accountId").toString(), "t39581066", "verifying the siteId ",test);
		validateValues(data.get("siteId").toString(), "1585077037", "verifying the siteId ",test);
		validateValues(data.get("ownerRole").toString(), "tenant", "verifying owner role",test);
		validateValues((Integer)data.get("uId"), 1223, "verifying uid",test);
		validateValues((Boolean)data.get("recFlag"), false, "verifying recFlag",test);
		validateValues(data.get("sentConfigOn").toString(), "1585081062468", "verifying sent config",test);
		validateValues(data.get("createdOn").toString(),"1585081062468", "verifying created on",test);
		//validateValues(data.get("isDisballed").toString(), null, "verifying is disballed",test);
		validateNullValues(data.get("isDisballed"), "verifying is disballed");
		
		test.log(LogStatus.PASS, " verified Blue reader details");

	}

}

