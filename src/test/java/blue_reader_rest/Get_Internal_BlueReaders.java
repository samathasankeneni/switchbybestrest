package blue_reader_rest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;


public class Get_Internal_BlueReaders extends Util{
	
	ExtentTest test;
	@Test
	public void get_internal_bluereader(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the Internal BlueReader details");
		
	    Response response = given().header("Authorization",getToken())
	            .queryParam("accountId","t39581066")
	            .queryParam("siteId","1585077037")
	            .get(ROOT_URI1+"/blueReader/internal");

	    System.out.println("The Response code is:"+response.getStatusCode());
	    System.out.println(response.asString());
	    Assert.assertEquals(response.getStatusCode(),200,"verifying the status code");
	    JsonPath jsonPath=response.jsonPath();
	    ArrayList keyValueList = jsonPath.get("data");
	    
	    test.log(LogStatus.PASS, " Displayed Internal Blue reader details");
	    
	}
	
}