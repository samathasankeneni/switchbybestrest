package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Export_BlueReader_Csv extends Util {

	ExtentTest test;
	@Test
	public void exportCSVFile(Method method) throws IOException {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to download the Csv file");
		
		
		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")

				.get("https://uat.switchbybest.com/accounts/api/blueReader/export-csv");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);

		JsonPath jsonPath = response.jsonPath();
		String responseUrl = jsonPath.get("data").toString().split("\\?")[0].replaceAll("%40", "@");
		String responseheaders = jsonPath.get("data").toString().split("\\?")[1];
		String headerarray[] = responseheaders.split("\\&");
		String xmzalgorithm = headerarray[0].split("=")[1];
		String xmzdate = headerarray[1].split("=")[1];
		String xmzsign = headerarray[2].split("=")[1];
		String xmzexpires = headerarray[3].split("=")[1];
		String xmzcredential = headerarray[4].split("=")[1].replaceAll("%2F", "/");
		System.out.println("xmzpred" + xmzcredential);
		String xmzsignature = headerarray[5].split("=")[1];

		System.out.println("xmzalg" + xmzalgorithm);
		System.out.println("====url" + responseUrl);
		Response response12 = given().queryParam("X-Amz-Algorithm", xmzalgorithm).queryParam("X-Amz-Date", xmzdate)
				.queryParam("X-Amz-SignedHeaders", xmzsign).queryParam("X-Amz-Expires", xmzexpires)
				.queryParam("X-Amz-Credential", xmzcredential).queryParam("X-Amz-Signature", xmzsignature)
				.get(responseUrl);

		System.out.println("The Response code is:" + response12.getStatusCode());
		System.out.println(response12.asString());
		String userdir = System.getProperty("user.dir");
		File outputFile = new File(userdir + "\\test-output", "switchreaders.csv");
		if (outputFile.exists()) {
			outputFile.delete();
		}
		System.out.println("Downloaded an " + response.getHeader("Content-Type"));

		byte[] fileContents = response12.asByteArray();
		OutputStream outStream = null;

		try {
			outStream = new FileOutputStream(outputFile);
			outStream.write(fileContents);
		} catch (Exception e) {
			System.out.println("Error writing file " + outputFile.getAbsolutePath());
		} finally {
			if (outStream != null) {
				outStream.close();
			}
		}

		test.log(LogStatus.PASS, " Downloaded the BlueReader_Csv File");
	}

}
