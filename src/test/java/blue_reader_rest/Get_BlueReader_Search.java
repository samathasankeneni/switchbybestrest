package blue_reader_rest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;


public class Get_BlueReader_Search extends Util{
	
	
	ExtentTest test;
	@Test
	public void bluereader_search(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the BlueReader details By Search");
		
	    Response response = given().header("Authorization",getToken())
	    		.queryParam("name","Switch Reader_3")
	            .queryParam("accountId","t39581066")
	            .queryParam("siteId","1585077037")
	            .get(ROOT_URI1+"/blueReader/search");

	    System.out.println("The Response code is:"+response.getStatusCode());
	    System.out.println(response.asString());
	    validateValues(response.getStatusCode(), 200, "verifying the status code", test);
	    JsonPath jsonPath=response.jsonPath();
	    HashMap data = jsonPath.get("data");
	    
	    ArrayList contentlist=(ArrayList)data.get("content");
	    HashMap eachMap=(HashMap)contentlist.get(0);
	    
	    /* validating values of content*/
		validateValues(eachMap.get("id").toString(),"5e7a6ae61964c40001f85524", "verifying the id",test);
		validateValues(eachMap.get("name").toString(), "Switch Reader_3", "verifying the name",test);
		validateValues((Boolean)eachMap.get("perimeterCheckin"), true, "verifying perimeterCheckin",test);
		validateValues((Boolean)eachMap.get("multifactor"), true, "verifying multifactor",test);
		validateValues(eachMap.get("accountId").toString(), "t39581066", "verifying the siteId ",test);
		validateValues(eachMap.get("siteId").toString(), "1585077037", "verifying the siteId ",test);
		validateValues(eachMap.get("ownerRole").toString(), "tenant", "verifying owner role",test);
		validateValues((Integer)eachMap.get("uId"), 1223, "verifying uid",test);
		validateValues((Boolean)eachMap.get("recFlag"), false, "verifying recFlag",test);
		validateValues(eachMap.get("sentConfigOn").toString(), "1585081062468", "verifying sent config",test);
		validateValues(eachMap.get("createdOn").toString(),"1585081062468", "verifying created on",test);
		validateNullValues(data.get("isDisballed"), "verifying is disballed");
		
		/* validating values of pageInfo*/
		HashMap pageInfo=(HashMap)data.get("pageInfo");
		validateValues(pageInfo.get("totalPages").toString(), "1", "verifying the total pages",test);
		validateValues(pageInfo.get("totalElements").toString(), "1", "verifying total elements",test);
		validateValues(pageInfo.get("size").toString(), "20", "verifying the size",test);
		validateValues(pageInfo.get("number").toString(),"0", "verifying the number",test);
		validateValues(pageInfo.get("numberOfElements").toString(), "1", "verifying number of elements",test);
		
		test.log(LogStatus.PASS, " verified Blue reader details");
	    
	}
	
}