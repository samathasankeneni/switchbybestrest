package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Switch_Reader_Set extends Util {

	private String switchDynamicName;
	private String dynamicId;
	private String uID;
	ExtentTest test;
	
	
	@Test(priority = 1)
	public void create_blueReader(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started the create a Blue Swictch Reader");
		

		int randomNumber = RandomUtils.nextInt(1, 1000);
		switchDynamicName = "Switch Reader_" + randomNumber;
		dynamicId = "5d892babb6686d0001ba" + randomNumber;

		String body = "{ \"accountId\": \"t39581066\", \"id\": \"" + dynamicId
				+ "\", \"multifactor\": true, \"name\": \"" + switchDynamicName + "\", \"ownerRole\": \"tenant\", "
				+ "\"perimeterCheckin\": true, \"recConfigOn\": \"2020-03-10T12:54:21.016Z\", \"recFlag\": true, \"sentConfigOn\": \"2020-03-10T12:54:21.016Z\", "
				+ "\"siteId\": 1585077037, \"uId\":0}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post("https://uat.switchbybest.com/accounts/api/blueReader");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		System.out.println((String)jsonPath.get("message"));
		test.log(LogStatus.PASS, "created blue Swicth reader");

	}

	@Test(priority = 2)
	
	
	public void get_bluereader(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to get the blue reader details");
		
		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037").get("https://uat.switchbybest.com/accounts/api/blueReader");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		System.out.println(response.asString());

		HashMap dataMap = jsonPath.get("data");
		ArrayList keyValueList1 = (ArrayList) dataMap.get("content");
		for (Object eachObject : keyValueList1) {
			HashMap eachMap = (HashMap) eachObject;
			String id = eachMap.get("id").toString();
			if (id.equals(dynamicId)) {
				System.out.println("id number:" + id);
				uID = eachMap.get("uId").toString();
				System.out.println("uID:" + uID);
				break;
			}
		}
		test.log(LogStatus.PASS, "blue reader details are displayed");
		
	}

	@Test(priority = 3)
	public void update_blueReader(Method method) {
		
		test=extent.startTest(method.getName()); 
		test.log(LogStatus.INFO,"Started to update the Blue reader");

		String body = "{ \"accountId\": \"t39581066\", \"id\": \"" + dynamicId + "\", \"multifactor\": true,"
				+ " \"name\": \"Switch Reader_8\", \"ownerRole\": \"tenant\", \"perimeterCheckin\": true, "
				+ "\"recConfigOn\": \"2020-03-06T18:57:39.428Z\", \"recFlag\": true, \"sentConfigOn\": \"2020-03-06T18:57:39.428Z\","
				+ " \"siteId\": \"1585077037\", \"uId\": \"" + uID + "\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/blueReader");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		System.out.println((String)jsonPath.get("message"));
		test.log(LogStatus.PASS, " updated Blue reader");
	}
	
	@Test(priority=4)
	public void delete_blureader(Method method) {
		
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the BlueReader Confitime");
		
		Response response = given().header("Authorization", getToken())
				.queryParam("uId", uID)
				.queryParam("accountId","t39581066")
				.queryParam("siteId","1585077037")
				.delete(ROOT_URI1 + "/blueReader");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath=response.jsonPath();
		HashMap dataMap=jsonPath.get("data");
		System.out.println(dataMap);
		System.out.println((String)jsonPath.get("message"));
		test.log(LogStatus.PASS, " updated Blue readers Confitime");

	}

}
