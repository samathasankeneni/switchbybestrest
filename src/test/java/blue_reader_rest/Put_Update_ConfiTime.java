package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_Update_ConfiTime extends Util{

	ExtentTest test;
	@Test
	public void update_confitime(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the BlueReader Confitime");
		
		String body= "{ \"bluereaderUid\": 1221, \"millis\": \"1660\", \"temperature\": 25}"
				+ " \"siteId\": \"1585077037\", \"uId\": \"1221\"}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/blueReader/config");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        Boolean dataMap = jsonPath.get("data");
        //Assert.assertEquals(jsonPath.get("data"),true);
        System.out.println(dataMap);
        
        test.log(LogStatus.PASS, " updated Blue readers Confitime");
        
	}
	
	
}


