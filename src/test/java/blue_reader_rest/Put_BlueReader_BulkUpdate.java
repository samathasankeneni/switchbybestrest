package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_BlueReader_BulkUpdate extends Util {

	ExtentTest test;
	@Test
	public void bulkUpdate(Method method) {
		

		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the BlueReader in bulk");
		
		String body = "{\t\"accountId\": \"t39581066\",\t\"siteId\": \"1585077037\",\t\"blueReaders\": "
				+ "[{\t\t\"id\": \" 5d892babb6686d0001ba7873\",\t\t\"name\": \"Switch Reader_1\",\t\t\"perimeterCheckin\": false,\t\t\"multifactor\": false,"
				+ "\t\t\"accountId\": \"t39581066\",\t\t\"siteId\": \"1585077037\",\t\t\"ownerRole\": \"Account Owner\",\t\t\"uId\": 1221,\t\t\"recFlag\": false,"
				+ "\t\t\"sentConfigOn\": 1583777580615,\t\t\"recConfigOn\": 1660,\t\t\"createdOn\": 1583523017404,\t\t\"lastUpdatedOn\": 1583852180866,\t\t\"isDisabled\": null\t},"
				+ " {\t\t\"id\": \"5e62ad931964c40001f84a45\",\t\t\"name\": \"Switch Reader_2\",\t\t\"perimeterCheckin\": false,\t\t\"multifactor\": true,"
				+ "\t\t\"accountId\": \"t39581066\",\t\t\"siteId\": \"1585077037\",\t\t\"ownerRole\": \"tenant\",\t\t\"uId\": 1222,\t\t\"recFlag\": false,"
				+ "\t\t\"sentConfigOn\": 1583777685740,\t\t\"recConfigOn\": 1660,\t\t\"createdOn\": 1583525267689,\t\t\"lastUpdatedOn\": 1583852180872,\t\t\"isDisabled\": null\t}]}";
		
		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/blueReader/bulk");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);

		
		 test.log(LogStatus.PASS, " Updated Blue readers in bulk");

	}

}
