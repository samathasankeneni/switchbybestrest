package blue_reader_Rest;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_BlueReader_Create extends Util{

	@Test
	public void post_bluereader_create() {
		
		String body= "{ \"accountId\": \"t17176877\", \"id\": \" 5d892babb6686d0001ba7873\", \"multifactor\": true,"
				+ " \"name\": \"Demo_123\", \"ownerRole\": \"Account Owner\", \"perimeterCheckin\": true, "
				+ "\"recConfigOn\": \"2020-03-06T18:57:39.428Z\", \"recFlag\": true, \"sentConfigOn\": \"2020-03-06T18:57:39.428Z\","
				+ " \"siteId\": \"1583158009\", \"uId\": \"4527\"}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).post(ROOT_URI1+"/blueReader");
		
		Assert.assertEquals(response.getStatusCode(),200,"verifying the status code");
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        System.out.println(jsonPath.get("message"));
        HashMap dataMap=jsonPath.get("data");
        System.out.println(dataMap.get("status"));
	}
	
	
}

