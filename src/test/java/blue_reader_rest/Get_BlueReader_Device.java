package blue_reader_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

	public class Get_BlueReader_Device extends Util {
		
		ExtentTest test;
		@Test
		public void get_bluereader_device(Method method) {
			
			test = extent.startTest(method.getName());
			test.log(LogStatus.INFO, "satrted to get the BlueReader details by Device");
			
		    Response response = given().header("Authorization",getToken())
		            .queryParam("accountId","t39581066")
		            .get(ROOT_URI1+"/blueReader/device");

		    System.out.println("The Response code is:"+response.getStatusCode());
		    System.out.println(response.asString());
		    validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		    JsonPath jsonPath=response.jsonPath();
		    HashMap dataList = jsonPath.get("data");
		    
		    test.log(LogStatus.PASS, " Blue reader by device details are dispalyed");
		    
		}

	}


