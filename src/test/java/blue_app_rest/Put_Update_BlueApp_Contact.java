package blue_app_rest;
import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_Update_BlueApp_Contact extends Util {

	ExtentTest test;

	@Test
	public void put_update_blueapp_contact(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to save the BlueApp contact");
		
		String body = "{ \"accountId\": \"t39581066\", \"createdBy\": \"Samathasankeneni\", \"email\": \"samathasankeneni@dormakaba.com\", "
				+ "\"highPerformanceBlueCore\": true, \"highPerformanceBlueReader\": true, \"id\": \"5e7a5e901964c40001f854f0\", "
				+ "\"locale\": \"MontrealQC\", \"name\": \"Rest_API\", \"phone\": \"514-896-5236\", \"siteId\": \"1585077037\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/blueApp/contactInfo");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		
		test.log(LogStatus.PASS, "Updated the BlueApp contact");
		
	}

}
