package blue_app_rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.HashMap;

public class Get_BlueApp_Contact extends Util {

	ExtentTest test;

	@Test
	public void get_blueapp_contact(Method method) {
		
		
		  ExtentTest test=extent.startTest(method.getName());
		  test.log(LogStatus.INFO, "Started to Get the blue app contact info");
		 
		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037").get(ROOT_URI1 + "/blueApp/contactInfo");

		System.out.println("The Response code is:");
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap keyValue = jsonPath.get("data");
		//test.log(LogStatus.PASS, "Displayed the Cron Account Info");
		
		validateValues(keyValue.get("id").toString(), "5e7a5e901964c40001f854f0", "verifying the id",test);
		validateValues(keyValue.get("siteId").toString(), "1585077037", "verifying the site id",test);
		validateValues(keyValue.get("accountId").toString(), "t39581066", "verifying the Account id",test);
		validateValues(keyValue.get("name").toString(), "Rest_API", "verifying the name",test);
		validateValues(keyValue.get("phone").toString(), "514-896-5236", "verifying the phone",test);
		validateValues(keyValue.get("locale").toString(), "MontrealQC", "verifying the locale",test);
		validateValues(keyValue.get("email").toString(), "samathasankeneni@dormakaba.com", "verifying the email",test);
		validateValues(keyValue.get("createdOn").toString(), "1585077904793", "verifying created on",test);
		validateValues(keyValue.get("createdBy").toString(), "Samatha sankeneni", "verifying the created by",test);
		validateValues(keyValue.get("lastUpdatedBy").toString(), "Samathasankeneni", "verifying the last update by",test);
		validateValues((Boolean)keyValue.get("highPerformanceBlueCore"), true, "verifying the highPerformanceBlueCore",test);
		validateValues((Boolean)keyValue.get("highPerformanceBlueReader"), true, "verifying the highPerformanceBlueReader",test);
		
		test.log(LogStatus.PASS, "BlueApp Contact Info details are displayed and validated");

	
	}

}
