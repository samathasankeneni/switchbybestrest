package blue_app_rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.HashMap;

public class Get_BlueApp_ContactForInternalCalling extends Util {

	ExtentTest test;

	@Test
	public void get_blueapp_contactfor_internalcalling(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to get the contact for internal calling details");
		
		
		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037").get(ROOT_URI1 + "/blueApp/contactInfoForInternalCall");

		System.out.println("The Response code is:");
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");

		/* Validating the details */

		validateValues(data.get("id").toString(), "5e7a5e901964c40001f854f0", "verifying the id",test);
		validateValues(data.get("siteId").toString(), "1585077037", "verifying the site id",test);
		validateValues(data.get("accountId").toString(), "t39581066", "verifying the Account id",test);
		validateValues(data.get("name").toString(), "KshUIwUgeKkXlG8V8fNttw==", "verifying the name",test);
		validateValues(data.get("phone").toString(), "OAoSloV8FwStY852DBYAsw==", "verifying the phone",test);
		validateValues(data.get("locale").toString(), "MontrealQC", "verifying the locale",test);
		validateValues(data.get("email").toString(), "6kpSm7CwUsby29mUxtkLPW26E79L6VzWEUMtca46rNc=", "verifying the email",test);
		validateValues(data.get("createdOn").toString(), "1585077904793", "verifying created on",test);
		validateValues(data.get("createdBy").toString(), "Samatha sankeneni", "verifying the created by",test);
		validateValues(data.get("lastUpdatedBy").toString(), "Samathasankeneni", "verifying the last update by",test);
		validateValues((Boolean)data.get("highPerformanceBlueCore"), true, "verifying the highPerformanceBlueCore",test);
		validateValues((Boolean)data.get("highPerformanceBlueReader"), true, "verifying the highPerformanceBlueReader",test);
		
		test.log(LogStatus.PASS, " Contact for InternalCalling details are displayed and validated");

	}

}
