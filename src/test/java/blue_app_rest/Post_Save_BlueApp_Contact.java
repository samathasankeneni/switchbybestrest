package blue_app_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Save_BlueApp_Contact extends Util {

	ExtentTest test;

	@Test
	public void post_save_blueapp_contact(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to save the BlueApp contact");

		String body = "{ \"accountId\": \"t39581066\", \"createdBy\": \"Samatha sankeneni\", \"email\": \"samatha.sankeneni@dormakaba.com\", "
				+ "\"highPerformanceBlueCore\": true, \"highPerformanceBlueReader\": true, \"locale\": \"Montreal QC\","
				+ " \"name\": \"Rest_API\", \"phone\": \"514-896-5236\", \"siteId\": \"1585077037\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/blueApp/contactInfo");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		//System.out.println(jsonPath.get("message"));
		
		test.log(LogStatus.PASS, "saved the BlueApp contact");
	}

}
