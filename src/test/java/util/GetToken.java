package util;

import static io.restassured.RestAssured.given;
import org.apache.commons.codec.binary.Base64;

//import java.util.Base64;
import java.util.HashMap;

import org.testng.annotations.Test;

import io.restassured.response.Response;

public class GetToken {

	private static final int Content = 0;

	@Test
	public void getToken() {

		// Response response=executeGetCall(ROOT_URI+"/blueApp",token);
		String usernameString = ("samatha.sankeneni@dormakaba.com" + ":" + "S@ndhya6");
		String usernameStringEncoded = new String(Base64.encodeBase64(usernameString.getBytes()));

		Response httpResponse = given().header("Authorization", "Basic " + usernameStringEncoded)
				.header("Accept", "application/vnd.com.nsn.cumulocity.user+json").when()
				.get("https://daveytest.us.cumulocity.com/user/currentUser?auth");

		System.out.println(httpResponse.getStatusCode());
		HashMap map = httpResponse.jsonPath().get("customProperties");
		String eTokenString = map.get("token").toString();

		String tmpAuth = "{\"eToken\":\"" + eTokenString
				+ "\",\"username\":\"samatha.sankeneni@dormakaba.com\",\"accountId\":\"t17176877\"}";
		System.out.println("Temp auth token:" + tmpAuth);
		String tempAuthEncoded = new String(Base64.encodeBase64(tmpAuth.getBytes()));

		System.out.println("Basic " + tempAuthEncoded);
		Response response = given().header("Authorization", "Basic " + tempAuthEncoded)
				.header("Content-Type", "application/x-www-form-urlencoded").header("charset", Content - 8)
				.post("https://uat.switchbybest.com/accounts/api/authenticate");
		System.out.println(response.statusCode());
		System.out.println((String)response.jsonPath().get("message"));
		System.out.println((String)response.jsonPath().get("fieldErrors"));
	}

}
