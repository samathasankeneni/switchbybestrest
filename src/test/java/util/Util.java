package util;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import org.apache.commons.codec.binary.Base64;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Random;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class Util extends ExtentReporterNG {

	/* URI's */

	public String ROOT_URI = "https://uat.switchbybest.com/events/api";
	public String ROOT_URI1 = "https://uat.switchbybest.com/accounts/api";

	public Response executeGetCall(String endpoint, String token) {
		Response response = given().header("Authorization", token).get(endpoint);
		System.out.println(response.asString());
		return response;

	}

	public Response executePostCall(String endpoint, String body, String token) {
		Response response = given().contentType(ContentType.JSON).accept(ContentType.JSON)
				.header("Authorization", "Bearer " + token).body(body).when().post(endpoint);
		System.out.println("POST Response\n" + response.asString());
		return response;
	}

	public Response executePutCall(String endpoint, String body) {
		Response response = given().contentType(ContentType.JSON).accept(ContentType.JSON).body(body).when()
				.put(endpoint);
		System.out.println("PUT Response\n" + response.asString());
		return response;
	}

	public Response executeDeleteCall(String endpoint) {
		Response response = delete(endpoint);
		System.out.println(response.asString());
		System.out.println(response.getStatusCode());
		return response;
	}

	/* code to generate Token */

	public String getToken() {

		String usernameString = ("sam-test" + ":" + "S@ndhya6");
		String usernameStringEncoded = new String(Base64.encodeBase64(usernameString.getBytes()));
		
		Response httpResponse = given().header("Authorization", "Basic " + usernameStringEncoded)
				.header("Accept", "application/vnd.com.nsn.cumulocity.user+json").when()
				.get("https://samtest.us.cumulocity.com/user/currentUser?auth");
		
		// System.out.println(httpResponse.getStatusCode());
		HashMap map = httpResponse.jsonPath().get("customProperties");
		String eTokenString = map.get("token").toString();
		String tmpAuth = "{\"eToken\":\"" + eTokenString
				+ "\",\"username\":\"sam-test\",\"accountId\":\"t39581066\"}";
		// System.out.println("Temp auth token:"+tmpAuth);
		String tempAuthEncoded = new String(Base64.encodeBase64(tmpAuth.getBytes()));
        //System.out.println("Encoded"+tempAuthEncoded);
		Response response = given().header("auth", "Basic " + tempAuthEncoded)
				.post("https://uat.switchbybest.com/accounts/api/authenticate");
		String bearerToken = response.jsonPath().get("id_token");
		System.out.println("Token:" + bearerToken);
		return bearerToken;
	}
	
	
	public String getStagingToken() {

		String usernameString = ("samatha.sankeneni@dormakaba.com" + ":" + "S@ndhya6");
		String usernameStringEncoded = new String(Base64.encodeBase64(usernameString.getBytes()));
		
		Response httpResponse = given().header("Authorization", "Basic " + usernameStringEncoded)
				.header("Accept", "application/vnd.com.nsn.cumulocity.user+json").when()
				.get("https://dormakabastaging.us.cumulocity.com/user/currentUser?auth");
		
		// System.out.println(httpResponse.getStatusCode());
		HashMap map = httpResponse.jsonPath().get("customProperties");
		String eTokenString = map.get("token").toString();
		String tmpAuth = "{\"eToken\":\"" + eTokenString
				+ "\",\"username\":\"samatha.sankeneni@dormakaba.com\",\"accountId\":\"t1037875\"}";
		// System.out.println("Temp auth token:"+tmpAuth);
		String tempAuthEncoded = new String(Base64.encodeBase64(tmpAuth.getBytes()));
		 //System.out.println("Encoded"+tempAuthEncoded);
		Response response = given().header("auth", "Basic " + tempAuthEncoded)
				.post("https://uat.switchbybest.com/accounts/api/authenticate");
		System.out.println("Status code:"+response.getStatusCode());
		String bearerToken = response.jsonPath().get("id_token");
		System.out.println("Token:" + bearerToken);
		return bearerToken;
	}
	
	public String getStagingToken1() {

		String usernameString = ("samatha.sankeneni@dormakaba.com" + ":" + "S@ndhya6");
		String usernameStringEncoded = new String(Base64.encodeBase64(usernameString.getBytes()));
		
		Response httpResponse = given().header("Authorization", "Basic " + usernameStringEncoded)
				.header("Accept", "application/vnd.com.nsn.cumulocity.user+json").when()
				.get("https://management.switchbybest.com/user/currentUser?auth");
		
		// System.out.println(httpResponse.getStatusCode());
		HashMap map = httpResponse.jsonPath().get("customProperties");
		String eTokenString = map.get("token").toString();
		String tmpAuth = "{\"eToken\":\"" + eTokenString
				+ "\",\"username\":\"samatha.sankeneni@dormakaba.com\",\"accountId\":\"t1037848\"}";
		// System.out.println("Temp auth token:"+tmpAuth);
		String tempAuthEncoded = new String(Base64.encodeBase64(tmpAuth.getBytes()));
		 //System.out.println("Encoded"+tempAuthEncoded);
		Response response = given().header("auth", "Basic " + tempAuthEncoded)
				.post("https://pre.switchbybest.com/accounts/api/authenticate");
		System.out.println("Status code:"+response.getStatusCode());
		String bearerToken = response.jsonPath().get("id_token");
		System.out.println("Token:" + bearerToken);
		return bearerToken;
	}
	
	
	

	/* code to validate the values */

	public void validateValues(String expected, String actual, String message,ExtentTest test) {
		System.out.println("Started :" + message);
		//Assert.assertEquals(expected, actual, message);
		if(expected.equals(actual)) {
			test.log(LogStatus.PASS, message);
			Assert.assertEquals(expected,actual);
		}else {
			test.log(LogStatus.FAIL, message);
			Assert.assertEquals(expected,actual);
		}
		
		System.out.println("Completed:" + message);
	}
	
	public void validateValues(int expected, int actual, String message,ExtentTest test) {
		System.out.println("Started :" + message);
		//Assert.assertEquals(expected, actual, message);
		if(expected==actual) {
			test.log(LogStatus.PASS, message);
			Assert.assertEquals(expected,actual);
		}else {
			test.log(LogStatus.FAIL, message);
			Assert.assertEquals(expected,actual);
		}
		System.out.println("Completed:" + message);
	}
	public void validateValues(double expected,double actual, String message,ExtentTest test) {
		System.out.println("Started :" + message);
		//Assert.assertEquals(expected, actual, message);
		if(expected==actual) {
			test.log(LogStatus.PASS, message);
			Assert.assertEquals(expected,actual);
		}else {
			test.log(LogStatus.FAIL, message);
			Assert.assertEquals(expected,actual);
		}
		System.out.println("Completed:" + message);
	}
	public void validateValues(boolean expected, boolean actual, String message,ExtentTest test) {
		System.out.println("Started :" + message);
		//Assert.assertEquals(expected, actual, message);
		if(expected==actual) {
			test.log(LogStatus.PASS, message);
			Assert.assertEquals(expected,actual);
		}else {
			test.log(LogStatus.FAIL, message);
			Assert.assertEquals(expected,actual);
		}
		System.out.println("Completed:" + message);
	}
	public void validateValues(float expected,float actual, String message,ExtentTest test) {
		System.out.println("Started :" + message);
		//Assert.assertEquals(expected, actual, message);
		if(expected==actual) {
			test.log(LogStatus.PASS, message);
			Assert.assertEquals(expected,actual);
		}else {
			test.log(LogStatus.FAIL, message);
			Assert.assertEquals(expected,actual);
		}
		System.out.println("Completed:" + message);
	}
	public void validateNullValues(Object value,String message ) {
		if(value==null) {
			Assert.assertNull(value,message);
		}
	}
	/* code to generate the random number */

	public static int generateRandomIntIntRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public void downloadFile(Response response,String downloadfile) throws IOException {
		JsonPath jsonPath = response.jsonPath();
		String responseUrl = jsonPath.get("data").toString().split("\\?")[0].replaceAll("%40", "@");
		String responseheaders = jsonPath.get("data").toString().split("\\?")[1];
		String headerarray[] = responseheaders.split("\\&");
		String xmzalgorithm = headerarray[0].split("=")[1];
		String xmzdate = headerarray[1].split("=")[1];
		String xmzsign = headerarray[2].split("=")[1];
		String xmzexpires = headerarray[3].split("=")[1];
		String xmzcredential = headerarray[4].split("=")[1].replaceAll("%2F", "/");
		System.out.println("xmzpred" + xmzcredential);
		String xmzsignature = headerarray[5].split("=")[1];

		System.out.println("xmzalg" + xmzalgorithm);
		System.out.println("====url" + responseUrl);
		Response response12 = given().queryParam("X-Amz-Algorithm", xmzalgorithm).queryParam("X-Amz-Date", xmzdate)
				.queryParam("X-Amz-SignedHeaders", xmzsign).queryParam("X-Amz-Expires", xmzexpires)
				.queryParam("X-Amz-Credential", xmzcredential).queryParam("X-Amz-Signature", xmzsignature)
				.get(responseUrl);

		System.out.println("The Response code is:" + response12.getStatusCode());
		System.out.println(response12.asString());
		String userdir = System.getProperty("user.dir");
		File outputFile = new File(userdir + "\\test-output",downloadfile);
		if (outputFile.exists()) {
			outputFile.delete();
		}
		System.out.println("Downloaded an " + response.getHeader("Content-Type"));

		byte[] fileContents = response12.asByteArray();
		OutputStream outStream = null;

		try {
			outStream = new FileOutputStream(outputFile);
			outStream.write(fileContents);
		} catch (Exception e) {
			System.out.println("Error writing file " + outputFile.getAbsolutePath());
		} finally {
			if (outStream != null) {
				outStream.close();
			}
		
		
		}
	
	}
	
}
