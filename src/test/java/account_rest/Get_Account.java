package account_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Account extends Util{
	ExtentTest test;
	@Test
	public void get_account_details(Method method) {
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to display the account details");

		Response response = given().header("Authorization", getStagingToken())
				.queryParam("SearchKey", "samtest")
				.queryParam("accountStatus", "ACTIVE")
				.get("https://uat.switchbybest.com/accounts/api/account");

		System.out.println("The Response code is:");
		System.out.println(response.asString());
		
		//Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		
		HashMap dataMap = jsonPath.get("data");
		test.log(LogStatus.PASS, " Account details displayed");

	}
		
	}
