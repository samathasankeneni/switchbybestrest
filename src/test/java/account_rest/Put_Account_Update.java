package account_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_Account_Update extends Util {

	ExtentTest test;

	@Test
	public void put_account_update(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to update the account");

		String body = "{\"id\":\"5e7a55741964c40001f854d0\",\"licenseKey\":\"OB2C-TBJN-YRE5-04V2\",\"accountId\":\"t39581066\",\"ownerRole\":\"admins\",\"accountName\":\"samtest\",\"domain\":\"samtest.us.cumulocity.com\",\"adminEmail\":\"samatha.sankeneni@dormakaba.com\",\"adminName\":\"sam-test\",\"contactName\":\"samatha sankeneni\",\"contactPhone\":\"+16175014002\",\"resetEmail\":false,\"accountStatus\":\"ACTIVE\",\"createdOn\":1585075572177,\"lastUpdatedOn\":1585159445082,\"company\":\"Dornakaba Digital\",\"blueReaderEnabled\":true,\"workOrder\":\"RestAssured test account\",\"adminPassword\":null,\"createdBy\":\"sam-test\",\"status\":\"ACTIVE\"}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/account");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		test.log(LogStatus.PASS, "updated the account");
	}

}
