package account_rest;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Post_Get_Account_License extends Util {

	ExtentTest test;

	@Test
	public void post_account_license(Method method) {
		
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started the Get the Acoount License details");
		
		Response response = given().header("Authorization", getToken()).post(ROOT_URI1 + "/account/license");
		System.out.println("The Response code is:");
		System.out.println(response.asString());
		Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		

//		validateValues(keyValue.get("id").toString(), "5d892ad2b6686d0001ba7823", "verifying the id",test);
//		validateValues(keyValue.get("noOfCredentials").toString(),"500", "verifying the credentials",test);
//
//		HashMap billing = (HashMap) keyValue.get("billing");

//	    validateNullValues(billing.get("farpointeCost"), "Verifying the farpoinntcost");
//		validateValues(billing.get("monthlyCost").toString(), "0.0", "verifying the monthlycost",test);
//		validateNullValues(billing.get("licenseCost"), "Verifying the licensecost");
//		validateValues(billing.get("yearlyCost").toString(), "2230.95", "verifying the yearlycost",test);
//		validateValues(billing.get("currentMonthCost").toString(), "910.02", "verifying the curent month cost",test);
//
//		validateValues(keyValue.get("licenseToken").toString(), "ECA9-FMGI-R8DZ-87VB", "verifying the liceneToken",test);
//		validateValues(keyValue.get("licenseStatus").toString(), "Claimed", "verifying the license status",test);
//		validateValues(keyValue.get("accoundId").toString(), "t17176877", "verifying the Accpunt id",test);
//		validateValues(keyValue.get("role").toString(), "admins", "verifying the role",test);
//		validateValues(keyValue.get("userId").toString(), "tony-davey", "verifying the user id",test);
//		validateValues(keyValue.get("accountName").toString(), "daveytest", "verifying the accountName",test);
//		validateValues(keyValue.get("createdOn").toString(), "1569270482023", "verifying created on",test);
//		validateValues(keyValue.get("claimedOn").toString(), "1569270662230", "verifying the claimed on",test);
//	
//
//		HashMap partner = (HashMap) keyValue.get("partner");
//		validateValues(partner.get("name").toString(), "Davey Test Org 2", "verifying the Name",test);
//		validateValues(partner.get("pocName").toString(), "+h3qn6sWCejTLzHoh13oiw==", "verifying the pocName",test);
//		validateValues((Boolean)partner.get("enabled"), true, "verifying enabled:",test);
//		validateValues(partner.get("pocEmail").toString(), "2z9ExrSiz1mhGKsNxjY6f0q4+K0bxvy4BD7R/q7ZhBI=",
//				"verifying the pocEmail",test);
//		validateValues(partner.get("pocTelephone").toString(), "K06LbNkozxOQ4t8yNzEiYg==", "verifying the pocTelephone",test);
//
//		HashMap address = (HashMap) partner.get("address");
//		validateValues(address.get("addressLine1").toString(), "1000 E", "verifying the address",test);
//		validateValues(address.get("addressLine2").toString(), "Test Address", "verifying the address line 2",test);
//		validateValues(address.get("city").toString(), "Indianapolis", "verifying the city:",test);
//		validateValues(address.get("state").toString(), "Indiana", "verifying the state",test);
//		validateValues(address.get("pincode").toString(), "46217", "verifying the pincode",test);
//		validateValues(address.get("country").toString(), "USA", "verifying the country",test);
//
//		validateValues(partner.get("id").toString(), "5d892aabb6686d0001ba7822", "verifying the id",test);
//		validateValues(partner.get("lastUpdatedBy").toString(), "tony-davey", "verifying last updated by",test);
//		validateValues(partner.get("createdBy").toString(), "tony-davey", "verifying the Name",test);
//		validateValues(partner.get("createdOn").toString(), "1569270443925", "verifying created on",test);
//		validateValues(partner.get("pocId").toString(), "p065020871", "verifying pocid",test);
//		validateValues((Boolean)partner.get("deleteFlag"), false, "verifying the deleteflag",test);		
//		validateNullValues(billing.get("partnerCost"), "Verifying the partnercost");
//		validateValues((Boolean)partner.get("isPartnerCost"), false, "verifying the partner cost",test);
//
//		validateValues((Boolean)keyValue.get("blueReaderEnabled"), true, "verifying if the blueReader Enabled",test);
		
		test.log(LogStatus.PASS, "Account license Displayed and validated");

	}

}
