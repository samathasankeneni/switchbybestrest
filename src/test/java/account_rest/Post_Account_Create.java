package account_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Account_Create extends Util {

ExtentTest test;
	@Test
	public void post_account_create(Method method) {
	
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to create an account");

		String body = "{\"company\":\"Dormakaba Digital\",\"domain\":\"samtest.us.cumulocity.com\",\"adminEmail\":\"samatha.sankeneni@dormakaba.com\",\"adminName\":\"sam-test\",\"adminPassword\":\"S@ndhya6\",\"confirmPassword\":\"S@ndhya6\",\"contactName\":\"samatha rao\",\"contactPhone\":\"+16175014012\",\"accountName\":\"samrest\",\"sendPasswordResetEmail\":true,\"storageLimitPerDevice\":0,\"licenseKey\":\"OB2C-TBJN-YRE5-04V2\",\"blueReaderEnabled\":true,\"workOrder\":\"RestAssured test account\",\"createdBy\":\"samatha.sankeneni@dormakaba.com\",\"ownerRole\":\"admins\",\"claimedOn\":\"2020-03-25T01:46:24.210+0000\"}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/account");

		//Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		test.log(LogStatus.PASS, " Error creating the account");
	}

}
