package account_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Set_Site_Quota extends Util {

	ExtentTest test;
	@Test(priority=1)
	public void set_site_quota(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to set the site Quota");

		String body = "{ \"accountId\": \"t39581066\", \"siteQuotaInfo\": [ { \"credentialNumber\": 13, \"siteId\": \"1585077037\", \"usedCredential\": 0 } ]}";
		Response response = given().header("Authorization", getStagingToken1()).contentType(ContentType.JSON).body(body)
				.post("https://uat.switchbybest.com/accounts/api/account/sitequota");

		Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
//		System.out.println(jsonPath.get("message"));
		
		test.log(LogStatus.PASS, "Site Quota set succesfully");

	}
	
}
