package account_rest;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import org.apache.commons.lang3.RandomUtils;
import org.testng.Assert;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Account_Set extends Util {

	private String accountDynamicName;
	private String siteID;
	ExtentTest test;
	@BeforeTest
    public void beforeSuite(ITestContext testcontext){
    	String userdir=System.getProperty("user.dir");
    	Date now = new Date();
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm");
    	String time = dateFormat.format(now);
    	File dir = new File(userdir+"\\test-output\\"+time);
    	if(!dir.exists())
    	{    	
    		dir.mkdir();
    	
    	}
    	String suiteName=testcontext.getSuite().getName();
        extent = new ExtentReports(userdir+"\\test-output" + File.separator +time+File.separator+ "ExtentReportsTestNG_"+suiteName+".html", true);
    }

    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
      // extent = new ExtentReports(outputDirectory + File.separator + "ExtentReportsTestNG.html", true);
      //  for (ISuite suite : suites) {
			/*
			 * Map<String, ISuiteResult> result = suite.getResults();
			 * 
			 * for (ISuiteResult r : result.values()) { ITestContext context =
			 * r.getTestContext(); buildTestNodes(context.getPassedTests(), LogStatus.PASS);
			 * buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
			 * buildTestNodes(context.getSkippedTests(), LogStatus.SKIP); }
			 */
			 
      //  }
        extent.flush();
//        extent.close();
    }

	@Test(priority = 1)
	public void post_create_site_info(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started the test create site info");

		int randomNumber = RandomUtils.nextInt(1, 1000);
		accountDynamicName = "Bluedemo_SAM_" + randomNumber;
		String body = "{\"accountId\": \"t39581066\" ,\"name\": \"" + accountDynamicName + "\" }";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post("https://uat.switchbybest.com/accounts/api/account/site");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		System.out.println("The site name is:"+accountDynamicName);
		test.log(LogStatus.PASS, "created the site");
	}

	@Test(priority = 2)
	public void get_site_info(Method method) {

		
		  test=extent.startTest(method.getName()); 
		  test.log(LogStatus.INFO,"Started to generate the site info");
		  
		 
		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")
				.get("https://uat.switchbybest.com/accounts/api/account/site");

		//Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList keyValueList = jsonPath.get("data");
		for (Object eachObject : keyValueList) {
			HashMap eachMap = (HashMap) eachObject;
			String accountName = eachMap.get("name").toString();
			if (accountName.equals(accountDynamicName)) {
				System.out.println("account name:" + accountName);
				siteID = eachMap.get("siteId").toString();
				System.out.println("Site ID:" + siteID);
				break;

			}
			
		}
		 test.log(LogStatus.PASS, "Generated site info"); 
	}

	@Test(priority = 3)
	public void put_update_site_info(Method method) {

		
		test=extent.startTest(method.getName()); 
		  test.log(LogStatus.INFO,"Started to update the site info");
		 

		String body = "{\"accountId\":\"t39581066\", \"currentDataFormatUnid\":\"1\", \"currentDataLayoutUnid\":\"1\",\"siteId\":\""
				+ siteID + "\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put("https://uat.switchbybest.com/accounts/api/account/internal/siteInfo");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		Boolean data = jsonPath.get("data");
		System.out.println(data);

		 test.log(LogStatus.PASS, "updated site info"); 
	}

	@Test(priority = 4)
	public void get_QA_site_info(Method method) {
		
		test=extent.startTest(method.getName()); 
		  test.log(LogStatus.INFO,"Started to get the QA site info");
		 

		Response response = given().header("Authorization", getToken()).queryParam("accountId", "t39581066")
				.queryParam("siteId", siteID).get(ROOT_URI1 + "/account/siteInfo");

		System.out.println("The Response code is:");
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap keyValue = jsonPath.get("data");
		validateValues(keyValue.get("siteId").toString(), siteID, "verifying the siteID", test);
		validateValues(keyValue.get("name").toString(), accountDynamicName, "verifying the name", test);
		validateValues(keyValue.get("currentDataFormatUnid").toString(), "1", "verifying the currentDataFormatUnid",
				test);
		validateValues(keyValue.get("currentDataLayoutUnid").toString(), "1", "verifying the currentDataLayoutUnid",
				test);

		 test.log(LogStatus.PASS, "Validated the site info details");

	}

	@Test(priority = 5)
	public void put_update_site_name(Method method) {

		
		test=extent.startTest(method.getName()); 
		  test.log(LogStatus.INFO,"Started to update the site name");
		 
		String body = "{\"accountId\":\"t39581066\", \"name\":\"Blue-sam\",\"siteId\":\"" + siteID + "\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put("https://uat.switchbybest.com/accounts/api/account/rename-site");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);

		 test.log(LogStatus.PASS, "updated the site name");
	}

	@Test(priority = 6)
	public void delete_Site_Info(Method method) {

		test=extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to delete the site");

		String body = "{ \"accountId\": \"t39581066\", \"siteId\": \"" + siteID + "\"}";
		Response response = given().contentType("application/json").header("Authorization", getToken()).body(body)
				.delete("https://uat.switchbybest.com/accounts/api/account/site");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		test.log(LogStatus.PASS, "Deleted the site");

	}
	@AfterTest
	public void afterTest() {
		extent.flush();
	}
}
