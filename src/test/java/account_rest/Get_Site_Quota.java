package account_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;


public class Get_Site_Quota extends Util {
	
	ExtentTest test;
	@Test
	public void get_site_Quota(Method method) {
		
		test = extent.startTest(method.getName());
	    test.log(LogStatus.INFO, "Started to get the site Quota");
	    
		Response response = given().header("Authorization", getToken())
							.queryParam("accountId", "t39581066")
							.get("https://uat.switchbybest.com/accounts/api/account/sitequota");

		System.out.println("The Response code is:");
		System.out.println(response.asString());
		//Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList keyValueList = jsonPath.get("data");
		HashMap eachMap=(HashMap)keyValueList.get(0);
		
		validateValues(eachMap.get("siteId").toString(), "1585077037", "verifying the siteId ",test);
		validateValues((Integer)eachMap.get("credentialNumber"), 13, "verifying the credential Number ",test);
		validateValues((Integer)eachMap.get("usedCredential"), 4, "verifying Used Credential details ",test);
		
		
		test.log(LogStatus.PASS, "Site Quota details are displayed");
	}
}
