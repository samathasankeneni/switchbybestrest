package account_rest;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;
import static io.restassured.RestAssured.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Get_Cron_Generator extends Util {

	ExtentTest test;

	@Test
	public void get_cron_generator(Method method) {
		
		
		  test = extent.startTest(method.getName());
		  test.log(LogStatus.INFO, "Started to generate the Cron_generator details");
		 
		Response response = given().header("Authorization", getToken()).get(ROOT_URI1 + "/account/cron");
		System.out.println("The Response code is:");
		System.out.println(response.asString());
		//Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList keyValueList = jsonPath.get("data");
		test.log(LogStatus.PASS, "Cron Generator details are displayed");

	}

}
