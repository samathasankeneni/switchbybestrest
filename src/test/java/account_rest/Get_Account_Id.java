package account_rest;

import io.restassured.response.Response;
import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;

public class Get_Account_Id extends Util {

ExtentTest test;

	@Test
	public void getaccount_id(Method method) {

		test = extent.startTest(method.getName());
	    test.log(LogStatus.INFO, "Started to get the account details");
	   // getToken1();
	    Response response = given().header("Authorization", getStagingToken()).get(ROOT_URI1 + "/account/t39581066");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		
		HashMap dataMap = jsonPath.get("data");
		
		System.out.println(dataMap);
		
		test.log(LogStatus.PASS, " AccountId details displayed");


}
}
