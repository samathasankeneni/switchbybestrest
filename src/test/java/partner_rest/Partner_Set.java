package partner_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Partner_Set extends Util {

	private String ID;
	private String pocID;
	ExtentTest test;

	/* Post save Partner*/
	@Test(priority = 1)
	public void save_partner(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to save partner details");

		String body = "{ \"address\": { \"addressLine1\": \"6600 kildare rd\", \"addressLine2\": \"Cotesaintluc\", \"city\": \"montreal\","
				+ " \"country\": \"canada\", \"pincode\": \"56897\", \"state\": \"Quebec\" }, \"createdBy\": \"samatha.sankeneni@dormakaba.com\", "
				+ "\"name\": \"Rest API\", \"pocEmail\": \"restapi@gmail.com\", \"pocName\": \"Rest Test\", \"pocTelephone\": \"+91456124789\", \"updatedBy\": \"samatha\"}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/partner");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		HashMap dataMap = jsonPath.get("data");
		// HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap.get("status"));
		System.out.println(dataMap.get("message"));

		test.log(LogStatus.PASS, "Partner Details saved ");

	}

	/* Get Partner Details*/
	
	@Test(priority = 2)
	public void get_partner(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the partner details ");

		Response response = given().header("Authorization", getStagingToken()).get(ROOT_URI1 + "/partner");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		for (Object eachObject : dataList) {
			HashMap eachMap = (HashMap) eachObject;
			String accountName = eachMap.get("name").toString();
			if (accountName.equals("Rest API")) {
				System.out.println("account name:" + accountName);
				ID = eachMap.get("id").toString();
				System.out.println("ID: " + ID);
				pocID = eachMap.get("pocId").toString();
				System.out.println("pocID: " + pocID);
				break;

			}

		}

		test.log(LogStatus.PASS, "Partner details are displayed");

	}

	/* Put Edit Partner*/
	
	@Test(priority = 3)
	public void Edit_partner(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to edit partner details");

		String body = "{ \"address\": { \"addressLine1\": \"6600 kildare rd\", \"addressLine2\": \"Cote-Saint-Luc\","
				+ " \"city\": \"montreal\", \"country\": \"canada\", \"pincode\": \"56897\", \"state\": \"Quebec\" }, "
				+ "\"enabled\": true, \"id\": \"" + ID + "\", \"lastUpdatedBy\": \"samatha\", \"name\": \"Rest API\","
				+ " \"pocEmail\": \"restassured@gmail.com\", \"pocId\": \"" + pocID
				+ "\", \"pocName\": \"Rest Test\", \"pocTelephone\": \"+91456124789\"}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/partner");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		HashMap dataMap = jsonPath.get("data");
		// HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap.get("status"));
		System.out.println(dataMap.get("message"));

		test.log(LogStatus.PASS, "Partner Information saved ");

	}

	/* Get Partner details by Id*/
	
	@Test(priority = 4)
	public void partner_details_byId(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the partner details by Id ");

		Response response = given().header("Authorization", getStagingToken()).get(ROOT_URI1 + "/partner/" + ID);

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);

		test.log(LogStatus.PASS, "Partner details are displayed");

	}
	
	/* Put update Partner Cost*/

	@Test(priority = 5)
	public void Updated_partner_Cost(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to update the partner cost");

		String body = "{ \"isGlobalCost\": true, \"partnerCost\": 100, \"pocIdList\": [ \"" + pocID + "\" ]}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/partner/cost");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);

		test.log(LogStatus.PASS, "Partner Cost Updated");

	}

	/* Put Partner Disable*/
	
	@Test(priority = 6)
	public void partner_disable(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to Disable the partner");

		String body = "{\"name\":\"Rest API\",\"pocName\":\"Rest Test\",\"enabled\":false,\"pocEmail\":\"restapi@gmail.com\","
				+ "\"pocTelephone\":\"+91456124789\",\"address\":{\"addressLine1\":\"6600 Kildare rd\",\"addressLine2\":\"Cote_Saint_Luc\","
				+ "\"city\":\"montreal\",\"state\":\"Quebec\",\"pincode\":\"56897\",\"country\":\"canada\"},\"id\":\""
				+ ID + "\","
				+ "\"lastUpdatedBy\":\"samatha\",\"createdBy\":\"samatha.sankeneni@dormakaba.com\",\"lastUpdatedOn\":1585326766438,"
				+ "\"createdOn\":1585321237480,\"pocId\":\"" + pocID
				+ "\",\"deleteFlag\":false,\"partnerCost\":100,\"isPartnerCost\":true}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/partner/disable");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		Boolean data = jsonPath.get("data");
		System.out.println(data);

		test.log(LogStatus.PASS, "Partner Disablled ");

	}
	
	/* Post Partner Enable*/

	@Test(priority = 7)
	public void Enable_Partner(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to Enable the Partner");

		String body = "{\"name\":\"Rest API\",\"pocName\":\"Rest Test\",\"enabled\":true,\"pocEmail\":\"restapi@gmail.com\","
				+ "\"pocTelephone\":\"91456124789\",\"address\":{\"addressLine1\":\"6600 kildare rd\",\"addressLine2\":\"Cote_Saint_Luc\","
				+ "\"city\":\"montreal\",\"state\":\"Quebec\",\"pincode\":\"56897\",\"country\":\"canada\"},\"id\":\""
				+ ID + "\","
				+ "\"lastUpdatedBy\":\"samatha\",\"createdBy\":\"samatha.sankeneni@dormakaba.com\",\"lastUpdatedOn\":1585327468280,"
				+ "\"createdOn\":1585321237480,\"pocId\":\"" + pocID
				+ "\",\"deleteFlag\":false,\"partnerCost\":null,\"isPartnerCost\":false}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/partner/enable");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		Boolean data = jsonPath.get("data");
		System.out.println(data);

		test.log(LogStatus.PASS, "Partner Enabled");

	}
	
	/*Delete Partner*/

	@Test(priority = 8)
	public void Delete_Partner(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to Enable the Partner");

		String body = "{\"name\":\"Rest API\",\"pocName\":\"Rest Test\",\"enabled\":true,\"pocEmail\":\"restassured@gmail.com\","
				+ "\"pocTelephone\":\"+91456124789\",\"address\":{\"addressLine1\":\"6600 kildare rd\",\"addressLine2\":\"Cote-Saint-Luc\","
				+ "\"city\":\"montreal\",\"state\":\"Quebec\",\"pincode\":\"56897\",\"country\":\"canada\"},\"id\":\""
				+ ID + "\","
				+ "\"lastUpdatedBy\":\"samatha.sankeneni@dormakaba.com\",\"createdBy\":\"samatha.sankeneni@dormakaba.com\","
				+ "\"lastUpdatedOn\":1585335424977,\"createdOn\":1585331401858,\"pocId\":\"" + pocID
				+ "\",\"deleteFlag\":false,\"partnerCost\":5,\"isPartnerCost\":false}";

		Response response = given().header("Authorization", getStagingToken()).contentType(ContentType.JSON).body(body)
				.delete(ROOT_URI1 + "/partner");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		Boolean data = jsonPath.get("data");
		System.out.println(data);

		test.log(LogStatus.PASS, "Partner Enabled");

	}

}
