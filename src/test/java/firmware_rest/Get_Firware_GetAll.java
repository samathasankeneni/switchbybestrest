package firmware_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Firware_GetAll extends Util {
	
	ExtentTest test;

	@Test
	public void getaccount_id(Method method) {

		test = extent.startTest(method.getName());
	    test.log(LogStatus.INFO, "Started to get the firmware details");
		Response response = given().header("Authorization", getStagingToken()).get(ROOT_URI1 + "/firmware");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		
		test.log(LogStatus.PASS, "All Firmware deatils are dipslayed");

	
	}
}
