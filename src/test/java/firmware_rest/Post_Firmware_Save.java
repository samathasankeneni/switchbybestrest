package firmware_rest;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Firmware_Save extends Util{
 
	ExtentTest test;

@Test
public void save_firmware() {
    String userdir = System.getProperty("user.dir");
    Response response = given().
             header("Content-Type","multipart/form-data").
            header("Authorization", getStagingToken())
            .multiPart("file",new File(userdir + "\\src\\test\\resources\\test_data\\BlueCore_OS-UC_0.10.1.9.bin"))
            .queryParam("data", "{\"accountId\":\"t39581066\",\"fileVersion\":\"0.10.1.9\",\"fileDescription\":\" Bluefob_QA\",\"fileDocumentation\": \"BlueFob_QA\",\"firmwareType\":\"\"}")
            .queryParam("deviceType", "BlueFob")
            .post("https://uat.switchbybest.com/accounts/api/firmware");
    System.out.println(response.getStatusCode());
	JsonPath jsonPath = response.jsonPath();
	
	HashMap dataMap = jsonPath.get("data");
	System.out.println(dataMap.get("status"));
	System.out.println(dataMap.get("message"));
}
}

