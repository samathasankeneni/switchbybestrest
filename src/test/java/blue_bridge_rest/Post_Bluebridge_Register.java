package blue_bridge_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Bluebridge_Register extends Util {

	ExtentTest test;

	@Test

	public void bluebridge_register(Method method) {

		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "Started to register for BlueBridge");

		String body = "{ \"accountId\": \"t39581066\", \"accountName\": \"samtest\", \"serialNumber\": \"1555682-18566\", \"siteId\": 1585077037, \"siteName\": \"Rest_API\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/bluebridge");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		

		test.log(LogStatus.PASS, "Registered for Blue Bridge");

	}

}
