package blue_bridge_rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import util.Util;
import static io.restassured.RestAssured.*;

import java.util.ArrayList;
import java.util.HashMap;


public class Get_Blue_Core extends Util{
	
	@Test
	public void get_blue_core() {
	    Response response = given().header("Authorization",getToken())
	            .queryParam("accountId","t17176877")
	            .queryParam("emailId","samathasankeneni@dormakaba.com")
	            .queryParam("username","samathasankeneni")
	            .queryParam("token","B7ZY-T8OU-IH76-65B7")
	            .queryParam("enabled",true)
	            .get("https://uat.switchbybest.com/accounts/api/bluebridge/blueCore");

	    System.out.println("The Response code is:"+response.getStatusCode());
	    System.out.println(response.asString());
	    Assert.assertEquals(response.getStatusCode(),200,"verifying the status code");
	    JsonPath jsonPath=response.jsonPath();
	    ArrayList dataList = jsonPath.get("data");
	    int listCounter=0;
	    for(Object eachObject:dataList) {
	        listCounter++;
	        HashMap eachMap=(HashMap) eachObject;
	        
	        if(listCounter==1) {
	            validateValues(eachMap.get("name"), "Door_Port0_Addr0", "verifying the name");
	            validateValues(eachMap.get("deviceId"), "7556973f-5520-4133-90dd-867e30fbd12f", "verifying the device id");
	            validateValues(eachMap.get("unid"), "12297", "verifying the unid");
	            validateValues(eachMap.get("deleteFlag"), false, "verifying the deleteFlag");
	            validateValues(eachMap.get("serialNumber"), null, "verifying the serial Number");
	            validateValues(eachMap.get("configUpdateTime").toString(), "1570033438000", "verifying the configUpdateTime");
	            validateValues(eachMap.get("accountId"), "t17176877", "verifying the accountId");
	            validateValues(eachMap.get("siteId"), "1569951019", "verifying siteId");
	            validateValues(eachMap.get("blueBridgeSerialNumber"), "1181004-04705", "verifying blueBridgeSerialNumber");
	            validateValues(eachMap.get("siteName"), "Test Name Test", "verifying the site name");
	            validateValues(eachMap.get("batteryPercentRemaining"), null, "verifying batteryPercentRemaining");
	            validateValues(eachMap.get("batteryVoltage"), null, "verifying the batteryVoltage");
	            validateValues(eachMap.get("temperature"), 21, "verifying the temperature");
	            validateValues(eachMap.get("createdOn").toString(),"1569952455707", "verifying created on");
	            validateValues(eachMap.get("lastUpdatedOn").toString(),"1571075939963", "verifying the last update on");
	            validateValues(eachMap.get("recFlag"), null, "verifying the recFlag");
	            
	        }else if(listCounter==2){
	            validateValues(eachMap.get("name"),"Zcore","verifying the name");
	            validateValues(eachMap.get("deviceId"),"ebe05217-079c-485e-a067-3edac91acf72","verifying the device id");
	            validateValues(eachMap.get("unid"),"6153","verifying the unid");
	            validateValues(eachMap.get("deleteFlag"),false,"verifying the deleteFlag");
	            validateValues(eachMap.get("serialNumber"),null,"verifying the serial Number");
	            validateValues(eachMap.get("configUpdateTime").toString(),"1583419596000","verifying the configUpdateTime");
	            validateValues(eachMap.get("accountId"),"t17176877","verifying the accountId");
	            validateValues(eachMap.get("siteId"), "1576231208" ,"verifying siteId");
	            validateValues(eachMap.get("blueBridgeSerialNumber"),"1191017-03072","verifying blueBridgeSerialNumber");
	            validateValues(eachMap.get("siteName"),"Shashi_Test","verifying the site name");
	            validateValues(eachMap.get("batteryPercentRemaining"),88,"verifying batteryPercentRemaining");
	            validateValues(eachMap.get("batteryVoltage"),2890,"verifying the batteryVoltage");
	            validateValues(eachMap.get("temperature"),27,"verifying the temperature");
	            validateValues(eachMap.get("createdOn").toString(), "1582195687727" ,"verifying created on");
	            validateValues(eachMap.get("lastUpdatedOn").toString(),"1583419682929","verifying the last update on");
	            validateValues(eachMap.get("recFlag"),true,"verifying the recFlag");
	            
	        }else if(listCounter==3){
	            validateValues(eachMap.get("name"),"Indianapolis - 6161","verifying the name");
	            validateValues(eachMap.get("deviceId"),"f258ca97-abc6-447b-b59b-7e9e21175a84","verifying the device id");
	            validateValues(eachMap.get("unid"),"34825","verifying the unid");
	            validateValues(eachMap.get("deleteFlag"),false,"verifying the deleteFlag");
	            validateValues(eachMap.get("serialNumber"),null,"verifying the serial Number");
	            validateValues(eachMap.get("configUpdateTime").toString(),"1582820093000","verifying the configUpdateTime");
	            validateValues(eachMap.get("accountId"),"t17176877","verifying the accountId");
	            validateValues(eachMap.get("siteId"), "1574351204" ,"verifying siteId");
	            validateValues(eachMap.get("blueBridgeSerialNumber"),"1181004-04704","verifying blueBridgeSerialNumber");
	            validateValues(eachMap.get("siteName"),"dormakaba - QA 1","verifying the site name");
	            validateValues(eachMap.get("batteryPercentRemaining"),90,"verifying batteryPercentRemaining");
	            validateValues(eachMap.get("batteryVoltage"),2918,"verifying the batteryVoltage");
	            validateValues(eachMap.get("temperature"),29,"verifying the temperature");
	            validateValues(eachMap.get("createdOn").toString(),"1582820093434" ,"verifying created on");
	            validateValues(eachMap.get("lastUpdatedOn").toString(),"1583259631826","verifying the last update on");
	            validateValues(eachMap.get("recFlag"),true,"verifying the recFlag");
	        }
	    }
	    
	}
}
	