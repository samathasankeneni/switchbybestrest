package blue_bridge_rest;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import util.Util;
import static io.restassured.RestAssured.*;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Get_Bluebridge_Blue_Core extends Util {

	ExtentTest test;

	@Test
	public void get_blue_core(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		  test.log(LogStatus.INFO, "Started to get the Blue core details");
		  
		Response response = given().header("Authorization", getToken())
				.queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037")
				.queryParam("emailId", "samatha.sankeneni@dormakaba.com")
				.queryParam("username", "sam-test")
				.queryParam("token", "ULXQ-B1TC-J4QU-3P67")
				.queryParam("enabled", true)
				.get("https://uat.switchbybest.com/accounts/api/bluebridge/blueCore");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		test.log(LogStatus.PASS, "Blue cores details are dispalyed");
		
		
		
	}
}
