package blue_bridge_rest;

import static io.restassured.RestAssured.given;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Register extends Util{

	@Test
	public void post_register() {
		
		String body= "{ \"accountId\": \"t17176877\", \"accountName\": \"Blue 0304\", \"serialNumber\": \"FGH4-GHT7-HJY7\", \"siteId\": 1583158009, \"siteName\": \"Bluecloud_QA\"}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).post(ROOT_URI1+"/bluebridge");
		
		Assert.assertEquals(response.getStatusCode(),200,"verifying the status code");
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        System.out.println(jsonPath.get("message"));
        HashMap dataMap=jsonPath.get("data");
	}
	
	
}
