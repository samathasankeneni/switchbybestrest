package blue_cloud_audit_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Audit_Create extends Util {

	ExtentTest test;

	@Test
	public void post_audit_create(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "satrted tovCreate An Audit");

		String body = "{ \"accountId\": \"t39581066\", \"activity\": \"Audit log\", \"message\": \"this is a test audit1\", \"siteId\": \"1585077037\", \"type\": \"device\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/audit");

		validateValues(response.getStatusCode(), 201, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		
		test.log(LogStatus.PASS, "Created an Audit");
	}

}

