package blue_cloud_audit_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Get_BlueCloud_Audit extends Util {

	ExtentTest test;

	@Test
	public void post_get_bluecloud_audit(Method method) {
		
		ExtentTest test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "satrted to get the Audit Details");
		

		String body = "{ \"accountId\": \"t39581066\", \"fromDate\": \"2020-03-06T17:01:04.090Z\", \"page\": 1, \"searchKey\": null, \"siteId\": \"1585077037\", \"size\": 10, \"toDate\": \"2020-03-06T17:01:04.090Z\", \"type\": \"device\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/audit/logs");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap dataMap = jsonPath.get("data");
		System.out.println(dataMap);
		
		test.log(LogStatus.PASS, " verified Blue_Cloud Audit");
		
		
	}

}
