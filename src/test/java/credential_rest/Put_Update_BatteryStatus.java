package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_Update_BatteryStatus extends Util {

	ExtentTest test;
	@Test
	public void update_blueReader(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the battery status");
		
		String body= "{ \"accountId\": \"t17176877\", \"accountName\":\"daveytest\", \"batteryPercentRemaining\": 89, "
				+ "\"batteryVoltage\": 200, \"claimedOn\": \"2020-03-10T16:33:20.170Z\", \"credentialRole\": \"user\","
				+ " \"deviceId\": null, \"deviceType\": \"phone\", \"registrationKey\": \"72FDN097FRCAJAG1\", \"vcpId\": 44849434}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/credential/battery-status");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        
        Boolean status=jsonPath.get("data");
        System.out.println(status);
        validateValues(status, true ,"Verify the battery update status", test);
        
	}
}
