package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_Update_PerimeterCheckin extends Util {
	ExtentTest test;
	@Test
	public void update_perimeterCheckin(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");
		
		String body= "{ \"accountId\": \"t17176877\", \"perimeterCheckin\": true, \"registrationKeys\": [ \"72FDN097FRCAJAG1\" ]}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/credential/checkin-update");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        HashMap data=jsonPath.get("data");
        System.out.println(data);
        System.out.println(jsonPath.get("message"));
        
	}

}
