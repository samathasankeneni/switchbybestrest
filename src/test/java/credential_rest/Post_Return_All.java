package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Return_All extends Util {

	ExtentTest test;
	@Test(priority = 1)
	public void generate_credentail(Method method) {
		//credentialNumber = generateRandomIntIntRange(21, 200);
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");

		String body = "{ \"accountId\": \"t39581066\", \"claimedOn\": \"2020-03-11T12:25:18.955Z\", \"createdBy\": \"samatha.sankeneni@dormakaba.com\","
				+ " \"credentialRole\": \"user\", \"noOfCredentials\": 3, \"ownerRole\": \"tenant\", \"siteId\": \"1585076968\", "
				+ "\"siteName\": \"Bluedemo_SAM\", " + "\"startingRange\":\"1\" }";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
	}
	
	@Test(priority = 2)
	public void return_credential(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to delete the credential");
		String body = "{ \"accountId\": \"t39581066\",  \"siteId\": \"1585076968\"}";

		Response response = given().header("Authorization", getToken())
				.queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585076968")
				.post(ROOT_URI1 + "/credential/returnAll");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);

	}

}
	





