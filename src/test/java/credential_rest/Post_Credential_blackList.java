package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Credential_blackList extends Util {
	
	
	ExtentTest test;
	@Test(priority=1)
	public void credential_blackList(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to blacklist the credentail");
		String statusToChange="unclaimed";
		String body= "[{ \"accountId\": \"t17176877\", \"credentialNumber\": 3, \"credentialRole\": \"user\", \"deviceType\": \"phone\", "
				+ "\"legicStatus\": null, \"perimeterCheckin\": false, \"registrationKey\": \"WJ6P59OU7Q4ULYBT\", \"siteId\": \"1583158009\","
				+ " \"status\": \""+statusToChange+"\", \"userId\": \"156\" }]";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).post(ROOT_URI1+"/credential/blacklist");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        ArrayList dataList=jsonPath.get("data");
        System.out.println(dataList);  
        HashMap dataMap=(HashMap) dataList.get(0);
        String status= dataMap.get("status").toString();
       
        Assert.assertNotEquals(status,"blacklisted");
        test.log(LogStatus.PASS, " blacklisted the credential");
        
       
	}
	@Test(priority=3)
	public void credential_blackList_1(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to blacklist the credentail");
		String statusToChange="claimed";
		String body= "[{ \"accountId\": \"t17176877\", \"credentialNumber\": 12, \"credentialRole\": \"user\", \"deviceType\": \"phone\", "
				+ "\"legicStatus\": null, \"perimeterCheckin\": false, \"registrationKey\": \"72FDN097FRCAJAG1\", \"siteId\": \"1583158009\","
				+ " \"status\": \""+statusToChange+"\", \"userId\": \"156\" }]";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).post(ROOT_URI1+"/credential/blacklist");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        ArrayList dataList=jsonPath.get("data");
        System.out.println(dataList);  
        HashMap dataMap=(HashMap) dataList.get(0);
        String status= dataMap.get("status").toString();
       
        Assert.assertEquals(status,"blacklisted");
        test.log(LogStatus.PASS, " blacklisted the credentials");
       
	}
	
	
	/*Put Revert Blacklist*/
	@Test(priority=2)
	public void revert_blackList(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to revert the blacklisted credentail");
		
		String body= "[{ \"accountId\": \"t17176877\", \"credentialNumber\": 3, \"credentialRole\": \"user\", \"deviceType\": \"phone\", "
				+ "\"legicStatus\": null, \"perimeterCheckin\": false, \"registrationKey\": \"WJ6P59OU7Q4ULYBT\", \"siteId\": \"1583158009\","
				+ " \"status\": \"blacklisted\", \"userId\": \"156\" }]";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/credential/blacklistrevert");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        ArrayList dataList=jsonPath.get("data");
        System.out.println(dataList);
        HashMap dataMap=(HashMap) dataList.get(0);
        String status= dataMap.get("status").toString();
        Assert.assertEquals(status,"claimed");
        test.log(LogStatus.PASS, " Reverted the blacklisted the credential");
        
        //HashMap data=(HashMap)dataList.get("data");
        
        //validateValues(dataList.get("accountId"),"t17176877","verifying the accountId");
	}

}
	
