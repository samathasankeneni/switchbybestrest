package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Credential_Search extends Util {
	
	ExtentTest test;
	@Test
	public void get_credential_search(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the all the credentails in the site");
		
		Response response = given().header("Authorization", getToken())
				.queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037")
				.get(ROOT_URI1 + "/credential/search");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		
		test.log(LogStatus.PASS, " Displayed all credentials in the site");
		
	}
}
