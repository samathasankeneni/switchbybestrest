package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Put_UpdateRole extends Util {
	ExtentTest test;
	@Test
	public void updaterole(Method method) {
		
		String body = "{ \"accountId\": \"t17176877\", \"accountName\": null, \"batteryPercentRemaining\": null, \"batteryVoltage\": null, "
				+ "\"claimedOn\": \"2020-03-11T18:04:29.901Z\", \"credentialRole\": \"installer\", \"deviceId\": null, \"deviceType\": \"phone\", "
				+ "\"registrationKey\": \"72FDN097FRCAJAG1\", \"vcpId\": 0}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/credential/role");

		
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		System.out.println(jsonPath.get("exceptionMessage"));
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);

	}

}
