package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_Site_Dashboard extends Util {
	
	ExtentTest test;
	@Test
	public void get_credential(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get site dashboard credentails");
		Response response = given().header("Authorization", getToken())
				.get(ROOT_URI1 + "/credential/site-dashboard");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		if(dataList.size()>0) {
		 Assert.assertTrue(true);
			test.log(LogStatus.PASS, " Displayed credentials details by site dashboard");
		}else {
			Assert.assertTrue(false);
			test.log(LogStatus.FAIL, " Fail to Displayed credentials details by site dashboard");
		}
		

	

	}
}