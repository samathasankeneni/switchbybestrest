package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Post_Get_ByCredential_Numbers extends Util {
	
	ExtentTest test;
	@Test
	public void post_getby_credentialnumbers(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get credentails details by Credential Numbers");

		String body = "{ \"accountId\": \"t39581066\", \"credNum\": [ 1,2,12,20 ], \"siteId\": \"1585077037\"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/site-credential");

		Assert.assertEquals(response.getStatusCode(), 200, "verifying the status code");
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		
		test.log(LogStatus.PASS, " All The Given Credential Numbers are displayed");
	}

}
