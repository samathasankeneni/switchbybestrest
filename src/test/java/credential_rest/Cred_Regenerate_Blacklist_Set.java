package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Cred_Regenerate_Blacklist_Set extends Util {
	private String reg_Key;
	private String reg_Keys;
	private String siteId = "1585077037";
	private String siteName = "Bluecloud_QA";
	private int credentialNumber;
	private int credentialNumber1;
	private String current_Status;
	ExtentTest test;
	String statusToChange = "unclaimed";

	/* Post Generate Credential */
	@Test(priority = 1)
	public void generate_credentail(Method method) {
		credentialNumber = generateRandomIntIntRange(21, 200);

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");

		String body = "{ \"accountId\": \"t39581066\", \"claimedOn\": \"2020-03-11T12:25:18.955Z\", \"createdBy\": \"samatha.sankeneni@dormakaba.com\","
				+ " \"credentialRole\": \"user\", \"noOfCredentials\": 1, \"ownerRole\": \"tenant\", \"siteId\": \""
				+ siteId + "\", \"siteName\": \"" + siteName + "\", " + "\"startingRange\":\"" + credentialNumber
				+ "\" }";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		System.out.println(reg_Key);
		test.log(LogStatus.PASS, " Created the credentail");
	}

	/* Post Credential BlackList */
	@Test(priority = 2)
	public void credential_blackList(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to blacklist the credentail");
		String statusToChange = "unclaimed";
		String body = "[{ \"accountId\": \"t39581066\", \"credentialNumber\": " + credentialNumber
				+ ", \"credentialRole\": \"user\", \"deviceType\": \"phone\", "
				+ "\"legicStatus\": null, \"perimeterCheckin\": false, \"registrationKey\": \"" + reg_Key
				+ "\", \"siteId\": \"1585077037\"," + " \"status\": \"" + statusToChange + "\", \"userId\": \"156\" }]";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/blacklist");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
//		HashMap dataMap = (HashMap) dataList.get(0);
//		String status = dataMap.get("status").toString();
//		System.out.println("=====Check Status:" + status);
//		//validateValues(status, "unclaimed", "checking whether unclaimed credentials can be blacklisted or not", test);

		test.log(LogStatus.PASS, " blacklisted the credential");

	}
	
	/*Get_BlackListed_Credentials*/
	@Test(priority=3)
	public void Get_blacklisted_credential(Method method) {
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the blacklisted credentails");
		
		Response response = given().header("Authorization", getToken())
				.queryParam("accountId", "t39581066")
				.queryParam("siteId", "1585077037")
				.queryParam("status", "blacklisted")
				.get(ROOT_URI1 + "/credential/search");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		test.log(LogStatus.PASS, " Blacklisted Credentials are Displayed");

	}
	

	/* Put BlackList Revert */
	@Test(priority = 4)
	public void revert_blackList(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to revert the blacklisted credentail");

		String body = "[{ \"accountId\": \"t39581066\", \"credentialNumber\": " + credentialNumber
				+ ", \"credentialRole\": \"user\", \"deviceType\": \"phone\", "
				+ "\"legicStatus\": null, \"perimeterCheckin\": false, \"registrationKey\": \"" + reg_Key
				+ "\", \"siteId\": \"1585077037\"," + " \"status\": \"blacklisted\", \"userId\": \"156\" }]";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/credential/blacklistrevert");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
//		HashMap dataMap = (HashMap) dataList.get(0);
//        String status= dataMap.get("status").toString();
//        validateValues(status, "claimed", "Revert BlackList option not visible in UI", test);
		
	}
	

	/* Post Generate Credential */
	@Test(priority = 5)
	public void generate_credentail1(Method method) {
		credentialNumber1 = generateRandomIntIntRange(21, 200);

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");

		String body = "{ \"accountId\": \"t39581066\", \"claimedOn\": \"2020-03-11T12:25:18.955Z\", \"createdBy\": \"samatha.sankeneni@dormakaba.com\","
				+ " \"credentialRole\": \"user\", \"noOfCredentials\": 1, \"ownerRole\": \"tenant\", \"siteId\": \""
				+ siteId + "\", \"siteName\": \"" + siteName + "\", " + "\"startingRange\":\"" + credentialNumber1
				+ "\" }";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Keys = datamap.get("registrationKey").toString();
		System.out.println(reg_Keys);
		test.log(LogStatus.PASS, " Created the credentail 1");

	}

	/*Post Credential Regenerate*/
	@Test(priority = 6)
	public void regenerate_credential(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to regenerate the credential");

		String body = "[ { \"accountId\": \"t39581066\", \"credentialNumber\": \"" + credentialNumber
				+ "\", \"credentialRole\": \"user\", "
				+ "\"deviceType\": \"phone\", \"legicStatus\": null, \"perimeterCheckin\": true, \"registrationKey\": \""
				+ reg_Key + "\", \"siteId\": \"1585077037\", \"status\": \"unclaimed\", \"userId\": 0 }]";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/regenerate");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		System.out.println("The registrationKey is:" + reg_Key);

		String status = datamap.get("status").toString();
		validateValues(status, "claimed", "checking whether unclaimed credentials can be regenerated or not", test);

		
	}
	
	
	/* Get Credential by number*/ 
	@Test(priority = 7)

	public void get_credentialby_num2(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentail details by Credentialnum");
		Response response = given().header("Authorization", getToken()).queryParam("siteId", "1585077037")
				.queryParam("credentialNumber", credentialNumber).get(ROOT_URI1 + "/credential/cred-num");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		reg_Key = data.get("registrationKey").toString();
		System.out.println(reg_Key);
		System.out.println("Generated registrationKey is:" + reg_Key);
		test.log(LogStatus.PASS, " Displayed the credentail details by credentail number");
	}
	
	/*Post Sites-Regenerate*/

	@Test(priority = 8)
	public void Regenerate_sites(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to regenerate multiple credentials");

		String body = "[ \"" + reg_Key + "\", \"" + reg_Keys + "\"]";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/sites-regenerate");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		Boolean data = jsonPath.get("data");
		System.out.println(data);
		validateValues(data, false, "checking whether invalid token can be regenerated or not", test);
		System.out.println("should regenerate Only Claimed credentails");


	}
	
	/* Get Credential by number*/ 

	@Test(priority = 9)

	public void get_credentialby_num(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentail details by Credentialnum");
		Response response = given().header("Authorization", getToken()).queryParam("siteId", "1585077037")
				.queryParam("credentialNumber", credentialNumber).get(ROOT_URI1 + "/credential/cred-num");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		reg_Key = data.get("registrationKey").toString();
		System.out.println(reg_Key);
		System.out.println("Generated registrationKey is:" + reg_Key);
		test.log(LogStatus.PASS, " credentail details by credentail number 1 are Displayed");
	}

	
	/* Get Credential by number*/ 
	@Test(priority = 10)

	public void get_credentialby_num1(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentail details by Credentialnum");
		Response response = given().header("Authorization", getToken()).queryParam("siteId", "1585077037")
				.queryParam("credentialNumber", credentialNumber1).get(ROOT_URI1 + "/credential/cred-num");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		reg_Keys = data.get("registrationKey").toString();
		System.out.println(reg_Keys);
		test.log(LogStatus.PASS, " credentail details by credentail number are Displayed");
	}

	
	/*Post Credential Return*/
	@Test(priority = 11)
	public void return_credential(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to delete the credential");
		String body = "{ \"accountId\": \"t39581066\", \"credentialInfoVM\": [ { \"accountId\": \"t39581066\", \"credentialNumber\": \""
				+ credentialNumber + "\","
				+ " \"credentialRole\": \"user\", \"deviceType\": \"phone\", \"legicStatus\": null, \"perimeterCheckin\": true,"
				+ " \"registrationKey\": \"" + reg_Key
				+ "\", \"siteId\": \"1585077037\", \"status\": \"Unclaimed\", \"userId\": 0 } ]}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/return");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		System.out.println(reg_Key);
		test.log(LogStatus.PASS, " Credentail deleted ");
	}
	
	
	/*Post Credential Return*/
	@Test(priority = 12)
	public void return_credential1(Method method) {

		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to delete the credential 1");
		String body = "{ \"accountId\": \"t39581066\", \"credentialInfoVM\": [ { \"accountId\": \"t39581066\", \"credentialNumber\": \""
				+ credentialNumber1 + "\","
				+ " \"credentialRole\": \"user\", \"deviceType\": \"phone\", \"legicStatus\": null, \"perimeterCheckin\": true,"
				+ " \"registrationKey\": \"" + reg_Keys
				+ "\", \"siteId\": \"1585077037\", \"status\": \"Unclaimed\", \"userId\": 0 } ]}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/return");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		System.out.println(reg_Key);
		test.log(LogStatus.PASS, " Credentail 1 is deleted ");
	}

}