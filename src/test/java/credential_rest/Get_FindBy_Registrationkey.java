package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Get_FindBy_Registrationkey extends Util{
	
	
	ExtentTest test;
	@Test
	public void get_by_registrationkey(Method method){
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentaials by registration key");
		Response response = given().header("Authorization", getToken())
								   .queryParam("registrationkey", "2SQ6RY2DXCFSGGZM")
								   .queryParam("status", "blacklisted")
								   .get(ROOT_URI1 + "/credential/registrationkey");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");

		test.log(LogStatus.PASS, " Displayed credentials details by registration key");
		
		
		
		
	}

}
