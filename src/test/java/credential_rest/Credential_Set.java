package credential_rest;

import static io.restassured.RestAssured.given;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import util.Util;

public class Credential_Set extends Util {

	private String reg_Key;
	private String siteId = "1585077037";
	private String siteName = "Bluecloud_QA";
	private int credentialNumber;
	private String current_Status;
	private String vcpId;
	ExtentTest test;

	/* create credential */

	@Test(priority = 1)
	public void generate_credentail(Method method) {
		credentialNumber = generateRandomIntIntRange(21, 200);
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");

		String body = "{ \"accountId\": \"t39581066\", \"claimedOn\": \"2020-03-11T12:25:18.955Z\", \"createdBy\": \"samatha.sankeneni@dormakaba.com\","
				+ " \"credentialRole\": \"user\", \"noOfCredentials\": 1, \"ownerRole\": \"tenant\", \"siteId\": \""
				+ siteId + "\", \"siteName\": \"" + siteName + "\", " + "\"startingRange\":\"" + credentialNumber
				+ "\" }";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		test.log(LogStatus.PASS, " Created the credentail");
		// reg_Key12=reg_Key;

	}

	/* update credential */
	@Test(priority = 2)
	public void update_credential(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the credential");
		String body = "{ \"accountId\": \"t39581066\", \"accountName\":null, \"batteryPercentRemaining\": 0, "
				+ "\"batteryVoltage\": null, \"claimedOn\": \"2020-03-10T16:33:20.170Z\", \"credentialRole\": \"user\","
				+ " \"deviceId\": null, \"deviceType\": \"phone\", \"registrationKey\": \"" + reg_Key
				+ "\", \"vcpId\": 0}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/credential");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();

		String data = jsonPath.get("data");
		System.out.println("Response:" + data);
		test.log(LogStatus.PASS, "updated the credential");
		
	}
	
	/*Get Find by Registration Key*/
	
	@Test(priority=3)
	public void get_by_registrationkey(Method method){
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentaials by registration key");
		Response response = given().header("Authorization", getToken())
								   .queryParam("registrationkey", ""+reg_Key+"")
								   .queryParam("status", "Claimed")
								   .get(ROOT_URI1 + "/credential/registrationkey");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		vcpId=data.get("vcpId").toString();
		System.out.println(vcpId);

		test.log(LogStatus.PASS, " Credentials details by registration key are displayed");
			
	}
	
	/*Put Update Role*/
	@Test(priority=4)
	public void updaterole(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the role ");
		
		String body = "{ \"accountId\": \"t39581066\", \"accountName\": null, \"batteryPercentRemaining\": null, \"batteryVoltage\": null, "
				+ "\"claimedOn\": \"2020-03-11T18:04:29.901Z\", \"credentialRole\": \"Installer\", \"deviceId\": null, \"deviceType\": \"phone\", "
				+ "\"registrationKey\": \""+reg_Key+"\", \"vcpId\":"+vcpId+"}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.put(ROOT_URI1 + "/credential/role");

		
		System.out.println(response.getStatusCode());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		Assert.assertNotNull(data.get("message"), "Verifying the message is null or not");
		//validateValues(data.get("message").toString(),"sucessfuly changed the role","verifying the role",test);
	}
	
	/*Update PerimeterCheckin*/
	
	@Test(priority=5)
	public void update_perimeterCheckin(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to generate the credential");
		
		String body= "{ \"accountId\": \"t39581066\", \"perimeterCheckin\": true, \"registrationKeys\": [ \""+reg_Key+"\" ]}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/credential/checkin-update");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        HashMap data=jsonPath.get("data");
        System.out.println(data);
        System.out.println(data.get("message"));
        validateValues(data.get("status").toString(), "true", "Verifying the status", test);
        validateValues(data.get("message").toString(), "Sucessfully updated perimeter checkin", "Verifying the message", test);
        
	}
	
	/*Put Update BatteryStatus*/
	
	@Test(priority=6)
	public void update_batteryStatus(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to update the battery status");
		
		String body= "{ \"accountId\": \"t39581066\", \"accountName\":\"daveytest\", \"batteryPercentRemaining\": 89, "
				+ "\"batteryVoltage\": 200, \"claimedOn\": \"2020-03-10T16:33:20.170Z\", \"credentialRole\": \"user\","
				+ " \"deviceId\": null, \"deviceType\": \"phone\", \""+reg_Key+"\": \"72FDN097FRCAJAG1\", \"vcpId\": "+vcpId+"}";
		
		Response response = given().header("Authorization",getToken()).contentType(ContentType.JSON).body(body).put(ROOT_URI1+"/credential/battery-status");
		

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
        System.out.println(response.getStatusCode());
        JsonPath jsonPath=response.jsonPath();
        
        Boolean status=jsonPath.get("data");
        System.out.println(status);
        validateValues(status, true ,"Verify the battery update status", test);
        
	}

/* Regenerate new registration token */
	
	@Test(priority = 7)
	public void regenerate_credential(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to regenerate the credential");

		String body = "[ { \"accountId\": \"t39581066\", \"credentialNumber\": \"" + credentialNumber
				+ "\", \"credentialRole\": \"user\", "
				+ "\"deviceType\": \"phone\", \"legicStatus\": null, \"perimeterCheckin\": true, \"registrationKey\": \""
				+ reg_Key + "\", \"siteId\": \"1585077037\", \"status\": \"Claimed\", \"userId\": 0 }]";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/regenerate");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();
		System.out.println("The registrationKey is:"+reg_Key);
		
		test.log(LogStatus.PASS, " Regenerated the credential");
	}

	/*get credential by cred number*/
	@Test(priority=8)
	public void get_credentialby_num(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentail details by Credentialnum");
		Response response = given().header("Authorization", getToken())
				.queryParam("siteId", "1585077037")
				.queryParam("credentialNumber",  credentialNumber )
				.get(ROOT_URI1 + "/credential/cred-num");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		reg_Key = data.get("registrationKey").toString();
		System.out.println("Generated registrationKey is:"+reg_Key);
		test.log(LogStatus.PASS, " Displayed the credentail details by credentail number");
	}
	
	/* regenerate new token */
	@Test(priority = 9)
	public void site_regenerate(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to regenerate the site credential");
		Response response = given().header("Authorization", getToken()).queryParam("registrationKey", "" + reg_Key + "")
				.post(ROOT_URI1 + "/credential/site-regenerate");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		Boolean data = jsonPath.get("data");
		System.out.println(data);
		validateValues(data, false, "checking whether invalid token can be regenerated or not", test);
		

	}
	
	/*get credential by cred number*/
	@Test(priority=10)
	public void get_credentialby_num1(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to get the credentail details by Credentialnum");
		
		Response response = given().header("Authorization", getToken())
				.queryParam("siteId", "1585077037")
				.queryParam("credentialNumber", credentialNumber)
				.get(ROOT_URI1 + "/credential/cred-num");

		System.out.println("The Response code is:" + response.getStatusCode());
		System.out.println(response.asString());
		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		JsonPath jsonPath = response.jsonPath();
		HashMap data = jsonPath.get("data");
		System.out.println(data);
		reg_Key = data.get("registrationKey").toString();
		System.out.println("Generated registrationKey is:"+ reg_Key);
		
		test.log(LogStatus.PASS, "Displayed the credentail details by credentail number");
	}
		

	/* Delete the credential */
	@Test(priority = 11)
	public void return_credential(Method method) {
		
		test = extent.startTest(method.getName());
		test.log(LogStatus.INFO, "started to delete the credential");
		String body = "{ \"accountId\": \"t39581066\", \"credentialInfoVM\": [ { \"accountId\": \"t39581066\", \"credentialNumber\": \""
				+ credentialNumber + "\", \"credentialRole\": \"user\", \"deviceType\": \"phone\", \"legicStatus\": null, \"perimeterCheckin\": true,"
				+ " \"registrationKey\": \"" + reg_Key
				+ "\", \"siteId\": \"1585077037\", \"status\": \"Unclaimed\", \"userId\": 0 } ]}";

		Response response = given().header("Authorization", getToken()).contentType(ContentType.JSON).body(body)
				.post(ROOT_URI1 + "/credential/return");

		validateValues(response.getStatusCode(), 200, "verifying the status code", test);
		System.out.println(response.getStatusCode());
		JsonPath jsonPath = response.jsonPath();
		ArrayList dataList = jsonPath.get("data");
		System.out.println(dataList);
		HashMap datamap = (HashMap) dataList.get(0);
		reg_Key = datamap.get("registrationKey").toString();	
		System.out.println("return registrationKey is:"+reg_Key);
		test.log(LogStatus.PASS, " Credentail deleted ");
	}

}
